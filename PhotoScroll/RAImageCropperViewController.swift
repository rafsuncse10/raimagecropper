//
//  RAImageCropperViewController.swift
//  PhotoScroll
//
//  Created by RAFAT TOUQIR RAFSUN on 9/1/16.
//  Copyright © 2016 raywenderlich. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



protocol RAImageCropperViewControllerDelegate:class {
  func croppedImage(_ image:UIImage?)
}


class RAImageCropperViewController: UIViewController {
  
  
  
  let scrollView : UIScrollView = {
    let scrollView = UIScrollView()
    //This patch is need to look better while didScroll get time to trigger
    scrollView.backgroundColor = .black
    return scrollView
  }()
  
  lazy var imageView: UIImageView = {
    let imageView = UIImageView()
    let imageToBeCropped = UIImage(named: self.photoName)
    imageView.frame.size = imageToBeCropped!.size
    imageView.image = imageToBeCropped
    return imageView
  }()
  
  var lastScale:CGFloat = 0
  
  let cropperView : UIImageView = {
    let view = UIImageView()
    let width = min(UIScreen.main.bounds.width,UIScreen.main.bounds.height) * (2.3 / 5)
    view.frame.size = CGSize(width: width, height: width)
    view.layer.borderWidth = 5
    view.layer.borderColor = UIColor.white.cgColor
    view.layer.cornerRadius = width/2
    view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    return view
  }()
  
  var croppedImage:UIImage?{
    didSet{
      //        cropperView.image = croppedImage
    }
  }
  
  lazy var backgroundView :UIView = {
    let view = UIView(frame: self.view.frame)
    view.backgroundColor = .lightGray
    return view
  }()
  
  
  lazy var visualEffectView : UIVisualEffectView = {
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    visualEffectView.frame = self.view.frame
    
    return visualEffectView
    
    
  }()
  
  
  var photoName: String!
  weak var delegate:RAImageCropperViewControllerDelegate?
  
  override func viewDidLoad() {
    
    
    scrollView.addSubview(imageView)
    imageView.boundInside(scrollView)
    
    self.view.addSubview(scrollView)
    scrollView.boundInside(self.view)
    
    
    scrollView.delegate = self
    setupGestureRecognizer()
    
    
    self.view.addSubview(visualEffectView)
    visualEffectView.boundInside(self.view)
    visualEffectView.isHidden = true
    
    self.view.addSubview(cropperView)
    cropperView.center = view.center
    
    
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_:)))
    view.addGestureRecognizer(pinchGesture)
    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
    panGesture.minimumNumberOfTouches = 1
    panGesture.maximumNumberOfTouches = 1
    cropperView.addGestureRecognizer(panGesture)
    
    
    let leftBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneTapped))
    let rightBarButtonItem = UIBarButtonItem(title: "Crop", style: .done, target: self, action: #selector(cropImageTapped))
    self.navigationItem.leftBarButtonItem = leftBarButtonItem
    self.navigationItem.rightBarButtonItem = rightBarButtonItem
    
  }
  
  
  func doneTapped() {
    
    
    delegate?.croppedImage(croppedImage)
    
    if self.navigationController?.viewControllers.count > 1{
      self.navigationController?.popViewController(animated: true)
    }else{
      self.dismiss(animated: true, completion: nil)
    }
    
  }
  
  
  //Cropping the image.
  
  func cropImage(_ image:UIImage,cropRegion:CGRect) -> UIImage? {
    
    if let subImage = image.cgImage?.cropping(to: cropRegion){
      
      let croppedImage = UIImage( cgImage:subImage)
      
      return croppedImage
    }
    return nil
  }
  
  
  func cropImageTapped() {
    
    let finalOrigin = CGPoint(x: scrollView.contentOffset.x + cropperView.frame.origin.x,y: scrollView.contentOffset.y + cropperView.frame.origin.y)
    
    let cropRegion = CGRect(origin: CGPoint(x: finalOrigin.x/scrollView.zoomScale,y: finalOrigin.y/scrollView.zoomScale),size: CGSize(width: cropperView.frame.size.width / scrollView.zoomScale,height: cropperView.frame.size.height / scrollView.zoomScale))
    
    let croppedImageTemp = cropImage(imageView.image!, cropRegion: cropRegion)
    croppedImage = croppedImageTemp
    
    
    // To fade in:
    self.visualEffectView.effect = UIBlurEffect(style: .light)
    self.visualEffectView.isHidden = false
    cropperView.image = croppedImageTemp
    UIView.animate(withDuration: 0.5, animations: {
      self.visualEffectView.effect = UIBlurEffect(style: .dark)
      self.cropperView.center = self.view.center
    }) 
    
    
  }
  
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    
    coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
      
      let orient = UIApplication.shared.statusBarOrientation
      
      switch orient {
      case .portrait,.landscapeLeft,.landscapeRight:
        let width = min(UIScreen.main.bounds.width,UIScreen.main.bounds.height) * (2.3 / 5)
        self.cropperView.frame.size = CGSize(width: width, height: width)
        self.cropperView.center = self.view.center
        
      default:
        break
      }
      
      }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
        
        
    })
    
    super.viewWillTransition(to: size, with: coordinator)
  }
  
  fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
    let widthScale = size.width / imageView.bounds.width
    let heightScale = size.height / imageView.bounds.height
    let minScale = min(widthScale, heightScale)
    
    scrollView.minimumZoomScale = minScale
    scrollView.zoomScale = minScale
    
  }
  
  fileprivate func updateConstraintsForSize(_ size: CGSize) {
    
    
    let imageViewSize = imageView.frame.size
    let scrollViewSize = scrollView.bounds.size
    let navTopPadding:CGFloat = 44
    
    let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
    let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
    
    scrollView.contentInset = UIEdgeInsets(top: verticalPadding + navTopPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    
    
  }
  
  
  func handlePanGesture(_ recognizer:UIPanGestureRecognizer) {
    
    
    recognizer.view!.layer.removeAllAnimations()
    
    cropperView.image = nil
    let translation = recognizer.translation(in: self.view)
    recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x,
                                          y: recognizer.view!.center.y + translation.y);
    recognizer.setTranslation(CGPoint.zero, in: view)
    visualEffectView.isHidden = true
    
    
    
    if (recognizer.state == .ended) {
      
      let velocity = recognizer.velocity(in: view)
      let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y));
      let slideMult = (magnitude / 200) ;
      
      let slideFactor = 0.1 * slideMult; // Increase for more of a slide
      var finalPoint = CGPoint(x: recognizer.view!.center.x + (velocity.x * slideFactor),
                                   y: recognizer.view!.center.y + (velocity.y * slideFactor));
      
      let midPointX = self.cropperView.bounds.midX
      // If too far right...
      if (finalPoint.x > self.view.bounds.size.width  - midPointX){
        finalPoint.x = self.view.bounds.size.width - midPointX
      }else if (finalPoint.x < midPointX){ 	// If too far left...
        finalPoint.x = midPointX
      }
      let midPointY = self.cropperView.bounds.midY
      // If too far down...
      let imageY = scrollView.contentInset.top + min(self.view.bounds.height,imageView.frame.height)
      if (finalPoint.y > imageY  - midPointY){
        finalPoint.y = imageY - midPointY
      }else if (finalPoint.y < scrollView.contentInset.top + midPointY){	// If too far up...
        finalPoint.y = scrollView.contentInset.top + midPointY
      }
      
      
      UIView.animate(withDuration: Double(slideFactor), delay: 0, options: [.curveEaseOut], animations: {
        recognizer.view!.center = finalPoint;
        }, completion: { completed -> Void in
          
          if let _ = self.croppedImage{
            
            
            //            // To fade out:
            //            UIView.animateWithDuration(0.3, animations: {
            //              self.visualEffectView.alpha = 0
            //            }) { (finished) in
            //              self.visualEffectView.hidden = finished
            //            }
            
            
            //            // To fade in:
            //            self.visualEffectView.alpha = 0
            //            self.visualEffectView.hidden = false
            //            UIView.animateWithDuration(0.3, delay: 1, options: [], animations: {
            //              self.visualEffectView.alpha = 1
            //              self.cropperView.image = imageCropped
            //
            //              }, completion: nil)
            //
            //
            
            
          }
          
      })
      
      
      
      
    }
    
    
  }
  
  
  func handlePinchGesture(_ gesture:UIPinchGestureRecognizer) {
    let point = gesture.location(in: view)
    
    if let view = view.hitTest(point, with: nil), view === cropperView{
      
      
      
      if(gesture.state == .began) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = gesture.scale
      }
      
      if (gesture.state == .began || gesture.state == .changed) {
        
        
        let currentScale:CGFloat = CGFloat((view.layer.value(forKeyPath: "transform.scale") as AnyObject).floatValue ?? 0)
        
        // Constants to adjust the max/min values of zoom
        let kMaxScale:CGFloat = 2.0;
        let kMinScale:CGFloat = 1.0;
        
        var newScale = 1 -  (lastScale - gesture.scale)
        newScale = min(newScale, kMaxScale / currentScale);
        newScale = max(newScale, kMinScale / currentScale);
        let transform = view.transform.scaledBy(x: newScale, y: newScale);
        view.transform = transform;
        lastScale = gesture.scale  // Store the previous scale factor for the next pinch gesture call
      }
      
      
      
    }
    
  }
  
  
  
  func setupGestureRecognizer() {
    let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
    doubleTap.numberOfTapsRequired = 2
    scrollView.addGestureRecognizer(doubleTap)
  }
  
  func handleDoubleTap(_ recognizer: UITapGestureRecognizer) {
    
    if (scrollView.zoomScale > scrollView.minimumZoomScale) {
      scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
    } else {
      scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateMinZoomScaleForSize(view.bounds.size)
  }
  
}


extension RAImageCropperViewController: UIScrollViewDelegate {
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  func scrollViewDidZoom(_ scrollView: UIScrollView) {
    updateConstraintsForSize(view.bounds.size)
  }
}


extension UIView{
  
  func addConstraintsWithFormat(_ format: String, views: UIView...) {
    
    var viewsDictionary = [String: UIView]()
    for (index, view) in views.enumerated() {
      let key = "v\(index)"
      viewsDictionary[key] = view
      view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
  }
  
  func boundInside(_ superView: UIView){
    
    self.translatesAutoresizingMaskIntoConstraints = false
    superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics:nil, views:["subview":self]))
    superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics:nil, views:["subview":self]))
    
  }
  
  //  func boundInsideForScrollView(superView: UIView){
  //
  //    self.translatesAutoresizingMaskIntoConstraints = false
  //    superView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-\(leftRightConst)-[subview]-\(leftRightConst)-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics:nil, views:["subview":self]))
  //    superView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-\(topBottomConst)-[subview]-\(topBottomConst)-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics:nil, views:["subview":self]))
  //
  //  }
  
}
