//
//  ThemeButton.swift
//  RAImageCropper
//
//  Created by RAFAT TOUQIR RAFSUN on 9/4/16.
//  Copyright © 2016 rafsun. All rights reserved.
//

import UIKit

@IBDesignable class ThemeButton: UIButton {
  
  
  @IBInspectable var whiteIndicator:Bool = true{
    didSet{
      self.activityIndicator.activityIndicatorViewStyle = whiteIndicator ? UIActivityIndicatorViewStyle.white : UIActivityIndicatorViewStyle.gray
    }
  }
  @IBInspectable var rightPaddingIndicator:CGFloat = 4{
    didSet{
      rightConstraint.constant = rightPaddingIndicator
      self.layoutIfNeeded()
    }
  }
  
  
  var showIndicator = false{
    
    didSet{
      if showIndicator{
        self.activityIndicator.startAnimating()
      }else{
        self.activityIndicator.stopAnimating()
      }
    }
  }
  
  
  fileprivate var rightConstraint:NSLayoutConstraint = NSLayoutConstraint()
  
  
  fileprivate lazy var activityIndicator:UIActivityIndicatorView = {
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    indicator.translatesAutoresizingMaskIntoConstraints = false
    indicator.hidesWhenStopped = true
    return indicator
  }()
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    addActivityIndicator()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addActivityIndicator()
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    addActivityIndicator()
  }
  
  fileprivate func addActivityIndicator() {
    
    
    self.layer.cornerRadius = 5
    self.layer.masksToBounds = true
    
    self.addSubview(activityIndicator)
    
    
    let topConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .top, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)
    rightConstraint = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: activityIndicator, attribute: .right, multiplier: 1.0, constant: rightPaddingIndicator)
    let bottomConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)
    let centerConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0)
    
    
    self.addConstraints([topConstraint,rightConstraint,bottomConstraint,centerConstraint])
    
    
  }
  
  
}
