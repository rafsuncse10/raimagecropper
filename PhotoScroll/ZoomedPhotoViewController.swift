/*
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

class ZoomedPhotoViewController: UIViewController {
  @IBOutlet weak var imageView: UIImageView!
  
  var lastScale:CGFloat = 0
  
  @IBOutlet weak var scrollView: UIScrollView!
  
  
  
  let cropperView : UIImageView = {
    let view = UIImageView()
    let width = UIScreen.main.bounds.width * (2 / 5)
    view.frame.size = CGSize(width: width, height: width)
    view.layer.borderWidth = 5
    view.layer.borderColor = UIColor.white.cgColor
    view.layer.cornerRadius = width/2
    view.backgroundColor = UIColor.black.withAlphaComponent(0.4)

    view.clipsToBounds = true
    view.isUserInteractionEnabled = true
    return view
  }()
  
  var croppedImage:UIImage?{
    didSet{
//        cropperView.image = croppedImage
    }
  }
  
  lazy var backgroundView :UIView = {
    let view = UIView(frame: self.view.frame)
    view.backgroundColor = .lightGray
    return view
  }()
  
  
  lazy var visualEffectView : UIVisualEffectView = {
    var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    visualEffectView.frame = self.view.frame
    
    return visualEffectView
    
    
  }()
  
  
  var photoName: String!
  
  override func viewDidLoad() {
    imageView.image = UIImage(named: photoName)
    scrollView.delegate = self
    setupGestureRecognizer()
    
    
    self.view.addSubview(visualEffectView)
    visualEffectView.boundInside(self.view)
    visualEffectView.isHidden = true
    
    self.view.addSubview(cropperView)
    cropperView.center = view.center
    
    let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_:)))
    view.addGestureRecognizer(pinchGesture)
    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
    panGesture.minimumNumberOfTouches = 1
    panGesture.maximumNumberOfTouches = 1
    cropperView.addGestureRecognizer(panGesture)
    
    
    
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(cropImageFromRegion))
    
    
    
  }
  
  
  //Cropping the image.
  
  func cropImage(_ image:UIImage,cropRegion:CGRect) -> UIImage? {
    
    if let subImage = image.cgImage?.cropping(to: cropRegion){
      
      let croppedImage = UIImage( cgImage:subImage)
      
      
      
      return croppedImage
    }
    return nil
  }
  
  
  func cropImageFromRegion() {
    
    self.view.subviews.forEach{ view in

      if view === backgroundView{
        backgroundView.removeFromSuperview()
      }
    }
    
    
    let finalOrigin = CGPoint(x: scrollView.contentOffset.x + cropperView.frame.origin.x,y: scrollView.contentOffset.y + cropperView.frame.origin.y)
    
    let cropRegion = CGRect(origin: CGPoint(x: finalOrigin.x/scrollView.zoomScale,y: finalOrigin.y/scrollView.zoomScale),size: CGSize(width: cropperView.frame.size.width / scrollView.zoomScale,height: cropperView.frame.size.height / scrollView.zoomScale))
    
    let croppedImageTemp = cropImage(imageView.image!, cropRegion: cropRegion)
    croppedImage = croppedImageTemp
    
    
    // To fade in:
    self.visualEffectView.alpha = 0
    self.visualEffectView.isHidden = false
    cropperView.image = croppedImageTemp
    UIView.animate(withDuration: 0.3, animations: {
      self.visualEffectView.alpha = 1
      self.cropperView.center = CGPoint(x: UIScreen.main.bounds.midX,y:UIScreen.main.bounds.midY)
    }) 
    
    
  }
  
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    
    coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
      
      let orient = UIApplication.shared.statusBarOrientation
      
      switch orient {
      case .portrait:
        self.cropperView.center = CGPoint(x: UIScreen.main.bounds.midX,y:UIScreen.main.bounds.midY)
        
      case .landscapeLeft,.landscapeRight:
        self.cropperView.center = CGPoint(x: UIScreen.main.bounds.midX,y:UIScreen.main.bounds.midY)
        
      default:
        self.cropperView.center = CGPoint(x: UIScreen.main.bounds.midX,y:UIScreen.main.bounds.midY)
        // Do something else
      }
      
      }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
        print("rotation completed")
    })
    
    super.viewWillTransition(to: size, with: coordinator)
  }
  
  fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
    let widthScale = size.width / imageView.bounds.width
    let heightScale = size.height / imageView.bounds.height
    let minScale = min(widthScale, heightScale)
    
    scrollView.minimumZoomScale = minScale
    scrollView.zoomScale = minScale
    
  }
  
  fileprivate func updateConstraintsForSize(_ size: CGSize) {
    
    //    let yOffset = max(0, (size.height - imageView.frame.height) / 2)
    //    imageViewTopConstraint.constant = yOffset
    //    imageViewBottomConstraint.constant = yOffset
    //
    //    let xOffset = max(0, (size.width - imageView.frame.width) / 2)
    //    imageViewLeadingConstraint.constant = xOffset
    //    imageViewTrailingConstraint.constant = xOffset
    //
    //    view.layoutIfNeeded()
    //
    
    let imageViewSize = imageView.frame.size
    let scrollViewSize = scrollView.bounds.size
    
    let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
    let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
    
    scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    
    
    
  }
  
  
  func handlePanGesture(_ recognizer:UIPanGestureRecognizer) {
    
    
    recognizer.view!.layer.removeAllAnimations()
    
    cropperView.image = nil
    let translation = recognizer.translation(in: self.view)
    recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x,
                                          y: recognizer.view!.center.y + translation.y);
    recognizer.setTranslation(CGPoint.zero, in: view)
    visualEffectView.isHidden = true
    
    
    
    if (recognizer.state == .ended) {
      
      let velocity = recognizer.velocity(in: view)
      let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y));
      let slideMult = (magnitude / 200) ;
      
      let slideFactor = 0.1 * slideMult; // Increase for more of a slide
      var finalPoint = CGPoint(x: recognizer.view!.center.x + (velocity.x * slideFactor),
                                   y: recognizer.view!.center.y + (velocity.y * slideFactor));
      //      finalPoint.x = min(max(finalPoint.x, 0), view.frame.width - CGRectGetMidX(recognizer.view!.bounds))
      //      finalPoint.y = min(max(finalPoint.y, 0), view.frame.height - CGRectGetMidY(recognizer.view!.bounds))
      
      let midPointX = self.cropperView.bounds.midX
      // If too far right...
      if (finalPoint.x > self.view.bounds.size.width  - midPointX){
        finalPoint.x = self.view.bounds.size.width - midPointX
      }else if (finalPoint.x < midPointX){ 	// If too far left...
        finalPoint.x = midPointX
      }
      let midPointY = self.cropperView.bounds.midY
      // If too far down...
      let imageY = scrollView.contentInset.top + min(self.view.bounds.height,imageView.frame.height)
      if (finalPoint.y > imageY  - midPointY){
        finalPoint.y = imageY - midPointY
      }else if (finalPoint.y < scrollView.contentInset.top + midPointY){	// If too far up...
        finalPoint.y = scrollView.contentInset.top + midPointY
      }
      
      
      UIView.animate(withDuration: Double(slideFactor), delay: 0, options: [.curveEaseOut], animations: {
        recognizer.view!.center = finalPoint;
        }, completion: { completed -> Void in
          
          if let _ = self.croppedImage{
            
            
//            // To fade out:
//            UIView.animateWithDuration(0.3, animations: {
//              self.visualEffectView.alpha = 0
//            }) { (finished) in
//              self.visualEffectView.hidden = finished
//            }
            
            
//            // To fade in:
//            self.visualEffectView.alpha = 0
//            self.visualEffectView.hidden = false
//            UIView.animateWithDuration(0.3, delay: 1, options: [], animations: {
//              self.visualEffectView.alpha = 1
//              self.cropperView.image = imageCropped
//              
//              }, completion: nil)
//            
//            
            
            
          }
          
      })
      
      
      
      
      
      
      
      
      
    }
    
    
    
    
    
    
    
    
  }
  
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
//    print("inset\(scrollView.contentInset)")
//    
//    print("offset\(scrollView.contentOffset)")
//    
//    print("frame \(imageView.frame)")
  }
  
  
  func animationDidFinish() {
    
    
  }
  
  
  func handlePinchGesture(_ gesture:UIPinchGestureRecognizer) {
    let point = gesture.location(in: view)
    
    if let view = view.hitTest(point, with: nil), view === cropperView{
      
      
      
      if(gesture.state == .began) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = gesture.scale
      }
      
      if (gesture.state == .began || gesture.state == .changed) {
        
        
        let currentScale:CGFloat = CGFloat((view.layer.value(forKeyPath: "transform.scale") as AnyObject).floatValue ?? 0)
        
        // Constants to adjust the max/min values of zoom
        let kMaxScale:CGFloat = 2.0;
        let kMinScale:CGFloat = 1.0;
        
        var newScale = 1 -  (lastScale - gesture.scale)
        newScale = min(newScale, kMaxScale / currentScale);
        newScale = max(newScale, kMinScale / currentScale);
        let transform = view.transform.scaledBy(x: newScale, y: newScale);
        view.transform = transform;
        lastScale = gesture.scale  // Store the previous scale factor for the next pinch gesture call
      }
      
      
      
    }
    
  }
  
  
  
  func setupGestureRecognizer() {
    let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
    doubleTap.numberOfTapsRequired = 2
    scrollView.addGestureRecognizer(doubleTap)
  }
  
  func handleDoubleTap(_ recognizer: UITapGestureRecognizer) {
    
    if (scrollView.zoomScale > scrollView.minimumZoomScale) {
      scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
    } else {
      scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    updateMinZoomScaleForSize(view.bounds.size)
  }
  
}


extension ZoomedPhotoViewController: UIScrollViewDelegate {
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  func scrollViewDidZoom(_ scrollView: UIScrollView) {
    updateConstraintsForSize(view.bounds.size)
  }
}
