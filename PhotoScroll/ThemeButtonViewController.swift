//
//  ThemeButtonViewController.swift
//  RAImageCropper
//
//  Created by RAFAT TOUQIR RAFSUN on 9/4/16.
//  Copyright © 2016 rafsun. All rights reserved.
//

import UIKit

class ThemeButtonViewController: UIViewController {

  @IBOutlet weak var buttonTheme: ThemeButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      buttonTheme.addTarget(self, action: #selector(buttonThemeTapped), for: .touchUpInside)
      
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func buttonThemeTapped(){
    
      buttonTheme.showIndicator = !buttonTheme.showIndicator
      
    }

}
